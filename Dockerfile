############################################################
# Dockerfile to build container images
# RSdudio with spark connecton to yarn - hadoop cluester
# (Spark 1.6.0 libs from cloudera)
# Based on rocker/rstudio
# Debian GNU/Linux 8
############################################################
# https://docs.docker.com/engine/reference/builder/
# https://github.com/rocker-org/rocker/wiki/Using-the-RStudio-image
# https://hub.docker.com/r/rocker/rstudio/
# https://docs.docker.com/engine/getstarted/step_four/#step-1-write-a-dockerfile
# https://www.digitalocean.com/community/tutorials/docker-explained-using-dockerfiles-to-automate-building-of-images
############################################################
##### TO BUILD #####
# > cd to/this/folder
# > docker build -t fabiomgpinheiro/sandbox .
##### TO RUN #####
# > docker run --name sandbox_instance -i -t fabiomgpinheiro/sandbox
# > docker run -d -e ALLOWED_USER='user' -v $USER_DIR:/home/user -p 10022:22 --name R_Spark_instance -v /etc/spark:/etc/spark:ro -v /etc/hive:/etc/hive:ro -v /etc/hadoop:/etc/hadoop:ro -v /opt/cloudera/parcels:/opt/cloudera/parcels:ro -v /etc/krb5.conf:/etc/krb5.conf fabiomgpinheiro/sandbox
## Launch an R terminal
# > docker run --rm -it --user rstudio rocker/rstudio /usr/bin/R
## EXEC
# > docker exec -it sandbox_instance bash
##### REMOVE #####
# > docker stop sandbox_instance
# > docker rm sandbox_instance
# > docker rmi fabiomgpinheiro/sandbox
##### COPY IMAGEM #####
# docker save -o . fabiomgpinheiro/sandbox
# docker load -i <path to image tar file>
##### TEST #####
# > docker build -t fabiomgpinheiro/sandbox .
# > docker run -d -p 8787:8787 -p 8788:8788 -p 8022:22 --name F1 fabiomgpinheiro/sandbox 
# > docker exec -it F1 bash
# > docker build -t fabiomgpinheiro/sandbox . && docker stop F1 && docker rm F1 && docker run -d  -p 8787:8787 -p 8788:8788 -p 8022:22 --name F1 fabiomgpinheiro/sandbox 
############################################################

#https://hub.docker.com/r/rocker/r-ver/~/dockerfile/
#https://hub.docker.com/r/rocker/rstudio-stable/~/dockerfile/
FROM rocker/rstudio:latest
#USER ${user:-root}
#ARG user
##USER $user

#FIXME ARG kerberos=EXAMPLE.COM
#FIXME ARG yarn_server=aaa.com

LABEL maintainer="fabiomgpinheiro@gmail.com" \
      version="0.1" \
      description="This docker image support the use for R to connect to a yarn cluster to submit spark jobs"




################## BEGIN INSTALLATION ######################

##RUN wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie" http://download.oracle.com/otn-pub/java/jdk/8u131-b11/d54c1d3a095b4ff2b6607d096fa80163/jdk-8u131-linux-x64.rpm \
##  && rpm -i jdk-8u131-linux-x64.rpm
##https://tecadmin.net/install-java-8-on-debian/
## See Cloudera recommendations https://www.cloudera.com/documentation/enterprise/release-notes/topics/rn_consolidated_pcm.html#pcm_jdk 
##RUN echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main \
##          \ndeb-src http://ppa.launchpad.net/webupd8team/java/ubuntu trusty main" > /etc/apt/sources.list.d/java-8-debian.list \
##  && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886 \
##  && apt-get update \
##  && apt-get install oracle-java8-installer <<< 'yes'

  ##  && apt-get install oracle-java8-set-default


ENV DEBIAN_FRONTEND=noninteractive
#RUN apt-get -y update && apt-get install -y libcurl-devel 
RUN apt-get -y update && apt-get install -y krb5-user && apt-get install -y default-jdk #openjdk-7-jdk


# R
RUN apt-get install -y libicu-dev libbz2-dev liblzma-dev libpcre3-dev #r-cran-rjava #This is for error: 'libjri.so' failed
RUN echo "install.packages('rJava')\ninstall.packages('RJDBC')\ninstall.packages('sparklyr')" >packagesToInstall.r && R CMD javareconf && Rscript packagesToInstall.r

# Python 2
RUN apt-get install -y python-dev python-pip && pip install -U pip setuptools
# Jupyter
RUN pip install jupyter && pip install JayDeBeApi

## Need to configure non-root user for jupyter
RUN  useradd jupyter \
  && echo "jupyter:jupyter" | chpasswd \
  && mkdir /home/jupyter \
  && chown jupyter:jupyter /home/jupyter \
  && addgroup jupyter staff \
  && mkdir -p /etc/services.d/jupyter \
  ##TODO use s6 for user management https://github.com/just-containers/s6-overlay
  && echo "c.NotebookApp.password = u'sha256::$(echo -n 'jupyter' | sha256sum | awk '{print $1}')' \
          \nc.NotebookApp.port = 8788 \
          \nc.NotebookApp.open_browser = False \
          \nc.NotebookApp.ip = '*'" > /etc/services.d/jupyter/jupyter_notebook_config.py \
  && echo '#!/bin/bash \
           \n exec /usr/local/bin/jupyter notebook --config /etc/services.d/jupyter/jupyter_notebook_config.py --allow-root /' \
           > /etc/services.d/jupyter/run
           ##\n sudo -u jupyter exec /usr/local/bin/jupyter notebook --config /jupyter_notebook_config.py /' \

##              && echo '#!/bin/bash \
##           \n rstudio-server stop' \
##           > /etc/services.d/rstudio/finish
#usermod -s /bin/bash rstudio

# Cloudera drives
RUN mkdir /opt/drivers/ \
  && wget http://downloads.cloudera.com/connectors/hive_jdbc_2.5.18.1050.zip -O hive_jdbc.zip \
  && wget https://downloads.cloudera.com/connectors/impala_jdbc_2.5.37.1057.zip -O impala_jdbc.zip \
  && unzip -j hive_jdbc.zip "2.5.18.1050 GA/Cloudera_HiveJDBC4_2.5.18.1050.zip" -d /opt/drivers/ \
  && unzip -j hive_jdbc.zip "2.5.18.1050 GA/Cloudera_HiveJDBC41_2.5.18.1050.zip" -d /opt/drivers/ \
  && unzip -j impala_jdbc.zip "2.5.37.1057 GA/Cloudera_ImpalaJDBC4_2.5.37.zip" -d /opt/drivers/ \
  && unzip -j impala_jdbc.zip "2.5.37.1057 GA/Cloudera_ImpalaJDBC41_2.5.37.zip" -d /opt/drivers/ \
  && rm -f hive_jdbc.zip impala_jdbc.zip \
  && for f in $(ls /opt/drivers/*.zip); do (out=$(echo $f | sed 's/.zip//') && unzip -j $f -d $out && rm $f) ; done \
  && for d in /opt/drivers/*/ ; do (cd "$d" && for f in $(ls $d*.jar); do echo $f; done | tr -s '\n' ':' | sed 's/.$/\n/' > classpath.txt); done

# REMOVE vim
RUN apt-get -y install vim

# Install sshd for pycharm remote python
RUN apt-get -y install openssh-server && mkdir -p /etc/services.d/sshd \
    && echo 'PermitRootLogin no' >> /etc/ssh/sshd_config \ 
    && echo 'DenyUsers rstudio' >> /etc/ssh/sshd_config \
    && echo 'Banner /etc/banner' >> /etc/ssh/sshd_config \
    && echo '#!/bin/bash \n service ssh start' > /etc/services.d/sshd/run

#    && echo '#!/bin/bash \n exec /usr/sbin/sshd -D' > /etc/services.d/sshd/run

COPY sssd.conf /etc/sssd/
COPY krb5.conf banner /etc/

RUN mkdir -m 777 /home/user \
    && apt-get -y install  libnss-ldap libpam-ldap ldap-utils sssd \
    && echo '#!/usr/bin/with-contenv bash' > /etc/cont-init.d/sssd \
    && echo 'echo "\naccess_provider = ldap\nldap_access_filter=$LDAP_ACCESS_FILTER" >> /etc/sssd/sssd.conf ' >> /etc/cont-init.d/sssd  \
    && mkdir -p /etc/services.d/sssd \    
    && echo '#!/bin/bash' > /etc/services.d/sssd/run \
    && echo 'service sssd start' >> /etc/services.d/sssd/run
    
################## BEGIN CONFIGURATION ######################
# RUN mkdir /spark && mkdir /etc/spark
# ADD spark_lib.tar / spark_config.tar / hive_config.tar / hadoop_config.tar /
#RUN find /spark -type f -exec sed -i 's/leo\(.\)\.xpand\.com/leonor\1.westeurope.cloudapp.azure.com/g' {} +

VOLUME ["/home/user"]

#FIXME ADD krb5.conf /etc/krb5.conf

RUN echo '#!/usr/bin/with-contenv bash' > /etc/cont-init.d/sudoers \
    && echo 'echo "ALL            ALL = (ALL) NOPASSWD: ALL" >> /etc/sudoers' >>/etc/cont-init.d/sudoers
ENV SPARK_HOME=/opt/cloudera/parcels/CDH/lib/spark SPARK_CONF_DIR=/etc/spark/conf.cloudera.spark_on_yarn

##################### INSTALLATION END #####################
#WORKDIR ~/
#ENTRYPOINT echo
#CMD "echo" "Docker is now runing!"

## Usage: EXPOSE [port]
#RStudio port 8787
#jupyter port 8788
#SSH port 22
EXPOSE 8787 8788 4040 8088 8042 22
